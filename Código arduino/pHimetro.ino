 
 /****************************************************
 
pHmtero - CoSensores (Sensores Comunitarios) 

https://gitlab.com/cosensores
https://www.facebook.com/cosensores/
https://www.instagram.com/cosensores/

Somos miembros de Universidades Nacionales trabajando junto a comunidades organizadas 
en el desarrollo de métodos para evaluar la presencia de contaminantes 
de manera sencilla en el territorio, acompañando acciones y procesos reivindicativos.

*****************************************************
Adaptación del codigo de DFRobot Gravity: Analog pH Sensor / Meter Kit V2, SKU:SEN0161-V2
https://wiki.dfrobot.com/Gravity__Analog_pH_Sensor_Meter_Kit_V2_SKU_SEN0161-V2
https://github.com/DFRobot/DFRobot_PH
 
 1. El codigo fue evaluado en Arduino Uno - IDE 1.0.5
 2. Para calibrar:
     CALIBRAR  -> ingresa al modo calibracio'n 
     PHCAL    -> calibracio'n de dos puntos en buffer pH=4.01 y pH=6.86
     SALIR     -> guarda los parametros y sale del modo de calibracio'n
 
****************************************************
Librerias control sensor temperatura: ds18b20
https://github.com/PaulStoffregen/OneWire
https://github.com/milesburton/Arduino-Temperature-Control-Library

****************************************************/

//temperatura
#include <OneWire.h>                
#include <DallasTemperature.h>
OneWire ourWire(2);                //conectar sensor temperatura a pin digital 2
DallasTemperature sensors(&ourWire); //declara una variable u objeto para nuestro sensor

//pH
#define SCOUNT  30           // sum of sample point
int analogBuffer[SCOUNT];    //store the sample voltage
int analogBufferIndex = 0;

#include "DFRobot_PH.h"
#include <EEPROM.h>

#define PH_PIN A0
float voltage,phValue,temperature = 25;
DFRobot_PH ph;

void setup()
{
    Serial.begin(9600);  
    ph.begin();
    
//temperatura
sensors.begin();   //Se inicia el sensor

}

void loop()
{
  
//temperatura  
sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
float temp= sensors.getTempCByIndex(0); //Se obtiene la temperatura en ºC

//pH

    static unsigned long timepoint = millis();
    if(millis()-timepoint>30U){                  //intervalo de tiempo 30seg
        timepoint = millis();
        temperature = temp;         //comentar para no levantar temperatura
        analogBuffer[analogBufferIndex] = analogRead(PH_PIN)/1024.0*5000;    //lee el voltaje y lo guarda en el buffer cada 40ms
     analogBufferIndex++;
     if(analogBufferIndex == SCOUNT)
         analogBufferIndex = 0;
        voltage = getMedianNum(analogBuffer,SCOUNT);   // obtiene un valor estable aplicando como filtro un valor medio
        //voltage = analogRead(PH_PIN)/1024.0*5000;  // lee voltaje directo
        phValue = ph.readPH(voltage,temperature);  // convierte voltaje a pH 
        
//imprime      
  
        Serial.print("    >>> ");
        Serial.print(temperature,1);
        Serial.println(" ^C <<<");  
        Serial.print("    >>>  pH: ");
        Serial.print(phValue,2);
        Serial.println(" <<<");
        Serial.println();
        Serial.println();
   
    }
    
 ph.calibration(voltage,temperature);           //proceso de calibracio'n por CMD serial
   }
    

int getMedianNum(int bArray[], int iFilterLen)
{
      int bTab[iFilterLen];
      for (byte i = 0; i<iFilterLen; i++)
      {
      bTab[i] = bArray[i];
      }
      int i, j, bTemp;
      for (j = 0; j < iFilterLen - 1; j++)
      {
      for (i = 0; i < iFilterLen - j - 1; i++)
          {
        if (bTab[i] > bTab[i + 1])
            {
        bTemp = bTab[i];
            bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
         }
      }
      }
      if ((iFilterLen & 1) > 0)
    bTemp = bTab[(iFilterLen - 1) / 2];
      else
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
      return bTemp;
}

