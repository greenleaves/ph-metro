/****************************************************
pHmtero - CoSensores (Sensores Comunitarios) 
*****************************************************
Adaptación del codigo de DFRobot Gravity: Analog pH Sensor / Meter Kit V2, SKU:SEN0161-V2
https://wiki.dfrobot.com/Gravity__Analog_pH_Sensor_Meter_Kit_V2_SKU_SEN0161-V2
https://github.com/DFRobot/DFRobot_PH 
*****************************************************/

#ifndef _DFROBOT_PH_H_
#define _DFROBOT_PH_H_

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define ReceivedBufferLength 10  //largo del buffer del CMD serial

class DFRobot_PH
{
public:
    DFRobot_PH();
    ~DFRobot_PH();
    void    calibration(float voltage, float temperature,char* cmd);  //calibracio'n por CMD serial
    void    calibration(float voltage, float temperature);
    float   readPH(float voltage, float temperature); // voltaje a valor de pH 
    void    begin();   //inicializacio'n

private:
    float  _phValue;
    float  _acidVoltage;
    float  _neutralVoltage;
    float  _voltage;
    float  _temperature;

    char   _cmdReceivedBuffer[ReceivedBufferLength];  //guardado en el CMD serial
    byte   _cmdReceivedBufferIndex;

private:
    boolean cmdSerialDataAvailable();
    void    phCalibration(byte mode); // proceso de calibracio'n, guarda parametros en el EEPROM
    byte    cmdParse(const char* cmd);
    byte    cmdParse();
};

#endif
